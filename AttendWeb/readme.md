# RFID考勤系統查詢網頁
這是一個使用Angular框架開發的前端專案。
負責串接資料庫及查詢差勤資料及呼叫Server端API。
以TCP與HTTP協定呼叫API，提供查詢服務。


### Prerequisites 项目使用条件

#### 硬體需求
    具備網路功能電腦

#### 軟體需求
    Ubuntu 18.x (推薦)    
	Apache2 HTTP Web 服務器
	

## Getting Started 使用指南
將網頁發佈到Server後可以拜訪網頁。

## 設置Server端網頁發佈程式
```
$ sudo apt update && sudo apt install apache2
```

## 發佈網頁專案

把網頁資料上傳到Server中，/var/www/html 目錄中

瀏覽應用：

打開網頁瀏覽器並訪問網址來查看運行中的 Angular 網頁。

