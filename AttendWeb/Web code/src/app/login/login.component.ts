import { LoggingcheckComponent } from './../loggingcheck/loggingcheck.component';
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../my-data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  //username: string = '';
  //password: string = '';
  //message: string = 'test';

  username: string = '';
  password: any = '';
  errorMessage: string = ' ';
  // dataSvc: any;

  // 在構造函數中注入DataService，並使用private修飾，這樣dataSvc就可以在整個組件中使用了
  constructor(private router: Router, private dataService: DataService) {}
  login(): void {
    switch (this.username) {
      //username=kkk
      case 'kkk':
        {
          if (this.username === 'kkk' && this.password === '123456') {
            //把username傳出去
            const dataToSend = this.username;
            this.dataService.updateData(dataToSend);
            // 登入成功後的操作，比如導航到另一個頁面
            this.router.navigateByUrl('/hridcheck/home');

            this.errorMessage = '登入成功';
          } else {
            this.errorMessage = '登入失敗，用戶名或密碼錯誤';
          }
        }
        break;
      //username=kkk
      //---//
      //username=admin
      case 'admin':
        {
          if (this.username === 'admin' && this.password === '123456') {
            //把username傳出去
            const dataToSend = this.username;
            this.dataService.updateData(dataToSend);
            // 登入成功後的操作，比如導航到另一個頁面
            this.router.navigateByUrl('/hridcheck/home');

            this.errorMessage = '登入成功';
          } else {
            this.errorMessage = '登入失敗，用戶名或密碼錯誤';
          }
        }
        break;
      //username=admin
      //---//
      //username=Sean
      case 'Sean':
        {
          if (this.username === 'Sean' && this.password === '123456') {
            //把username傳出去
            const dataToSend = this.username;
            this.dataService.updateData(dataToSend);
            // 登入成功後的操作，比如導航到另一個頁面
            this.router.navigateByUrl('/hridcheck/home');

            this.errorMessage = '登入成功';
          } else {
            this.errorMessage = '登入失敗，用戶名或密碼錯誤';
          }
        }
        break;
      //username=Sean
      default:
    }
  }
}

/*
    {
      if (this.username === 'admin' && this.password === '123456') {
        //把username傳出去
        const dataToSend = this.username;
        this.dataService.updateData(dataToSend);
        // 登入成功後的操作，比如導航到另一個頁面
        this.router.navigateByUrl('/hridcheck/home');

        this.errorMessage = '登入成功';
      } else {
        this.errorMessage = '登入失敗，用戶名或密碼錯誤';
      }
    }
*/
