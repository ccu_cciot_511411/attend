import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private dataSource = new BehaviorSubject<any>(null); // 可以設定初始值
  currentData = this.dataSource.asObservable();

  constructor() {}

  // 更新資料的方法，任何組件都可以呼叫這個方法來更新資料
  updateData(data: any) {
    this.dataSource.next(data);
  }
}
