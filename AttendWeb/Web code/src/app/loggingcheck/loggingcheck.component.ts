import { Component, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
@Component({
  selector: 'app-loggingcheck',
  templateUrl: './loggingcheck.component.html',
  styleUrls: ['./loggingcheck.component.css'],
})
export class LoggingcheckComponent {
  private usersUrl = 'assets/user.json'; // users.json 放在 assets 資料夾中,使用者帳號密碼

  constructor(private http: HttpClient) {}

  getUsers(): Observable<any> {
    return this.http.get(this.usersUrl);
  }
}
