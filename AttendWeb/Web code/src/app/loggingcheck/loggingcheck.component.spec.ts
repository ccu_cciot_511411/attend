import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggingcheckComponent } from './loggingcheck.component';

describe('LoggingcheckComponent', () => {
  let component: LoggingcheckComponent;
  let fixture: ComponentFixture<LoggingcheckComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoggingcheckComponent]
    });
    fixture = TestBed.createComponent(LoggingcheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
