import { Component } from '@angular/core';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css'],
})
export class CalendarComponent {
  date: Date = new Date();
  mymonth = this.date.getMonth();
  myday = Number(this.date.getDate());
  myweak = this.date.getDay();
  mytime = this.date.getHours() + '時' + this.date.getMinutes() + '分';
  /*接收API回拋的JSON*/
  item = [
    { d: '2023/11/01', t: '08:00', t2: '17:00', status: '正常', tips: '' },
    { d: '2023/11/01', t: '08:00', t2: '17:00', status: '正常', tips: '' },
    { d: '2023/11/01', t: '08:00', t2: '17:00', status: '正常', tips: '' },
    { d: '2023/11/01', t: '08:00', t2: '17:00', status: '正常', tips: '' },
    { d: '2023/11/01', t: '08:00', t2: '17:00', status: '正常', tips: '' },
  ];
  /*接收API回拋的JSON*/
}
