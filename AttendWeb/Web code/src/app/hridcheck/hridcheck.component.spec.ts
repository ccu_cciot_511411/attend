import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HridcheckComponent } from './hridcheck.component';

describe('HridcheckComponent', () => {
  let component: HridcheckComponent;
  let fixture: ComponentFixture<HridcheckComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HridcheckComponent]
    });
    fixture = TestBed.createComponent(HridcheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
