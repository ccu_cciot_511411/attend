import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalendarComponent } from './calendar/calendar.component';
import { HridcheckComponent } from './hridcheck/hridcheck.component';
import { PageComponent } from './page/page.component';
import { LoginComponent } from './login/login.component';
import { ChecklogComponent } from './checklog/checklog.component';

//定義Router路徑
const routes: Routes = [
  {
    //設定第一層的路徑
    path: 'login',
    component: LoginComponent,
    pathMatch: 'full',
  },
  {
    //設定第一層的路徑
    path: 'hridcheck',
    component: HridcheckComponent,
    //設定子路徑
    children: [
      {
        path: 'home',
        component: PageComponent,
      },
      {
        path: 'calendar',
        component: CalendarComponent,
      },
      {
        path: 'checklog',
        component: ChecklogComponent,
      },
      //預設子路徑的預設頁面是導向home的Component
      { path: '', redirectTo: 'home', pathMatch: 'full' },
    ],
  },
  //預設一開始進入網頁的路徑，直接導向login的Component
  { path: '', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
