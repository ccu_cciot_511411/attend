import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../my-data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css'],
})
export class PageComponent implements OnInit, OnDestroy {
  username: string = '';

  private subscription: Subscription = new Subscription();

  constructor(private dataService: DataService) {}

  ngOnInit() {
    // 把login的username接收過來
    this.subscription.add(
      this.dataService.currentData.subscribe((updatedData: string) => {
        this.username = updatedData;
      })
    );
  }

  // 當組件銷毀時取消所有訂閱
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
