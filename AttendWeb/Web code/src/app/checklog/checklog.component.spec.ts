import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChecklogComponent } from './checklog.component';

describe('ChecklogComponent', () => {
  let component: ChecklogComponent;
  let fixture: ComponentFixture<ChecklogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChecklogComponent]
    });
    fixture = TestBed.createComponent(ChecklogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
