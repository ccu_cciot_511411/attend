import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, Injectable, OnInit, OnDestroy } from '@angular/core';

import { DataService } from '../my-data.service';
import { Subscription } from 'rxjs';

//定義JSON格式
export interface LogDetail {
  attend: string | null;
  leave: string | null;
  work_time: number;
  records: string[];
  holiday: string | null;
  valid: boolean;
}
//把JSON資料全部陣列
export interface Attendance {
  [date: string]: LogDetail;
}

@Component({
  selector: 'app-checklog',
  templateUrl: './checklog.component.html',
  styleUrls: ['./checklog.component.css'],
})
export class ChecklogComponent implements OnInit, OnDestroy {
  //宣告一個Attendance陣列格式的變數
  logitem: Attendance = {};
  //data: any;
  user: string = ' '; // 這裡將會從某處獲得使用者名稱
  startTime: string = '';
  endTime: string = '';
  errorMessage: string = '';

  private subscription: Subscription = new Subscription();
  constructor(private http: HttpClient, private dataService: DataService) {}

  ngOnInit() {
    // 把login的username接收過來
    this.subscription.add(
      this.dataService.currentData.subscribe((updatedData: string) => {
        this.user = updatedData;
      })
    );
  }

  // 當組件銷毀時取消所有訂閱
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  //get_log方法，接收網頁傳來的日期參數
  get_log(startTime: string, endTime: string): void {
    //定義要送出的get參數
    let params = new HttpParams()
      .set('user_id', this.user)
      .set('start_time', startTime + '-00:00:00')
      .set('end_time', endTime + '-23:59:59');
    //使用HTTP的get method呼叫API
    this.http
      .get('http://52.198.1.216:5566', {
        params,
        observe: 'response', // 使用 observe: 'response' GET完整的 HTTP 回覆
        responseType: 'text', // 指定傳回的字串為text
      })
      .subscribe((response) => {
        // 把回傳的TEXT類型轉為string
        const responseText = response.body as string;
        // 找到 JSON 字串格式的起始位置
        const jsonStartIndex = responseText.indexOf('{');
        if (jsonStartIndex !== -1) {
          // 截取 JSON資料部分
          const jsonResponseText = responseText.substring(jsonStartIndex);
          // 解析 JSON 資料
          const jsonData = JSON.parse(jsonResponseText);
          // 现在 jsonData 包含了有效的 JSON 資料
          this.logitem = jsonData;
        } else {
          // 如果未找到 JSON 資料，可以處理error
          console.error('JSON data not found in the response.');
          this.errorMessage = '沒有打卡紀錄唷!!!!';
        }
      });
  }
}
//this.http.get<Attendance>('assets/log.json').subscribe((data) => {
/*  //點下按鈕，就用http去讀取json資料
  get_log(startTime: string, endTime: string): void {
    let params = new HttpParams()
      .set('startTime', startTime + '-23:59:59')
      .set('endTime', endTime + '-23:59:59');

    this.http
      .get<Attendance>('http://52.198.1.216:5566', { params })
      .subscribe((data) => {
        this.logitem = data;
      });
  }
  
      this.socketService.getLog(startTime, endTime).subscribe((data) => {
      this.logitem = data;
    });
  */
