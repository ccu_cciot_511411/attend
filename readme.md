# RFID考勤系統

利用RFID Reader與雲端主機實現考勤紀錄、管理。

本系統串接了RFID Reader、Arduino、服務端以及AWS Dynamodb，由Arduino與RFID Reader結合作為設備端，搭配提供雲端支持的服務端，實現考勤紀錄、
查詢等功能。

## Getting Started 使用指南

### 請先確保具備以下硬體設備

#### Reader端
1. Arduino Wifi (Wemos D1 R1)
2. RFID Reader (MFRC522)

#### Server端
1. 具備聯網功能的Server


### 安裝

[Reader安裝說明](/AttendRFIDReader/readme.md)  
[Server端安裝說明](/AttendServer/readme.md)  
[Website安裝說明](/AttendWeb/readme.md)



