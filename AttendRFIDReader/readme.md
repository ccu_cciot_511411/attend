
簡介：
此檔案為Arduino的RFID的readme檔，主要是說明怎麼在Arduino IDE運行Wifi_Rfid_send_1228.ino程式

建置：
Wifi_Rfid_send_1228.ino為使用arduino IDE撰寫的程式，在運行程式時需要先安裝Arduino的環境
安裝環境的教學請參閱此網站https://www.minwt.com/arduino/22305.html，或是於arduino官網安裝
安裝Arduino IDE後須安裝 WeMos D1 WIFI 物聯網開發板的驅動程式https://www.techbang.com/posts/77602-wemos-d1-wifi-iot-board-driver
以及需要安裝arduino IDE ESP8266WiFi、MFRC522的函式庫，詳細步驟請參閱https://blog.jmaker.com.tw/arduino-rfid/

執行步驟：
當環境建置完成，於Arduino IDE開啟Wifi_Rfid_send_1228.ino，並且選擇Wemos D1 R1開發版，以及選擇插入USB連接埠，這裡我選擇COM4
於Arduino IDE上需選擇115200鲍率

電腦配置：
我這邊使用的環境為Window11，Arduino的IDE版本為2.2.1，但理論上Arduino於window及linux都可以運行