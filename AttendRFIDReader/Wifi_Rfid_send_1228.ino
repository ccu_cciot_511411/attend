#include <ESP8266WiFi.h>

#include <SPI.h>
#include <MFRC522.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define RST_PIN         D9          
#define SS_PIN          D10  //就是模組上的SDA接腳
#define buzzer          D7

MFRC522 mfrc522;   // 建立MFRC522實體


String card_ID;
//設定WIFI連接資訊
char* ssid = "Sean's wifi";
char* passwd = "0937@654760";
//設定連接IP及PORT
const uint16_t port = 7788;
const char * host = "52.198.1.216"; // ip,52.63.96.76
WiFiClient client;
 
void setup() {
  pinMode(buzzer,OUTPUT); //設定蜂鳴器角位為OUTPUTPIN
 Serial.begin(115200);  //設定傳輸資料的龅率

 WiFi.mode(WIFI_STA); //設定WIFI模式
 WiFi.begin(ssid, passwd);//設定WIFI密碼
 
 Serial.println("connecting to router... ");
    //等待wifi连接成功
 while (WiFi.status() != WL_CONNECTED) 
    {
 Serial.print(".");
 delay(500);
    }
 Serial.println("");
 Serial.print("WiFi connected, local IP address:");//連接成功後輸出訊息
 Serial.println(WiFi.localIP());
 
 delay(500);

  SPI.begin();        // 初始化SPI介面
  mfrc522.PCD_Init(SS_PIN, RST_PIN); // 初始化MFRC522卡
  Serial.print(F("Reader "));
  Serial.print(F(": "));
  mfrc522.PCD_DumpVersionToSerial(); // 顯示讀卡設備的版本
}
 
void loop() {

Serial.println("嘗試訪問伺服器");
if (client.connect(host, port)) //嘗試訪問目標地址 
{
Serial.println("訪問成功"); 
//client.print("Hello world!"); //向伺服器發送數據
  SPI.begin();        // 初始化SPI介面
  mfrc522.PCD_Init(SS_PIN, RST_PIN); // 初始化MFRC522卡
  if (mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial()) {
      // 顯示卡片內容
      Serial.print(F("Card UID:"));
      String myString = dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size); // 使用dump_byte_array將CARD的ID轉成字串
      Serial.println(myString);//輸出卡片ID字串
      client.print(myString);//透過WIFI傳輸至卡片ID至SERVER
      byte *id = mfrc522.uid.uidByte;   // 取得卡片的UID (第20行)
      byte idSize = mfrc522.uid.size;   // 取得UID的長度

      Serial.print(F("PICC type: "));
      MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
      Serial.println(mfrc522.PICC_GetTypeName(piccType));  //顯示卡片的類型

      mfrc522.PICC_HaltA();  // 卡片進入停止模式
    }
delay(1000);

while ( client.available()) //如果已連接或有收到的未讀取的數據 client.connected() ||
{
if (client.available()) //如果有數據可讀取 
{
String line = client.readStringUntil('\n'); //讀取數據到換行符 
Serial.print("讀取到數據："); 
Serial.println(line); 
 if (line == "777") //若SERVER回傳777代表合格卡 蜂鳴器逼一聲
    {
      tone(buzzer,1000, 200);
      delay(1000);
    }   
   if (line == "999") //若SERVER回傳999代表無此卡或無效卡 蜂鳴器逼二聲
  {
      tone(buzzer,330, 200);
      delay(400);
      tone(buzzer,330, 200);
      delay(400);
  }   

}
}

  
Serial.println("關閉當前連接"); 
client.stop(); //關閉客戶端 
} else 
{ 
  Serial.println("訪問失敗"); //若連接SERVER失敗則輸出訪問失敗
  client.stop(); //關閉客戶端 
  } delay(5000); 
  }



/**
 * 這個副程式把讀取到的UID，用16進位顯示出來
 */
String dump_byte_array(byte *buffer, byte bufferSize) {
  String result = "";
  for (byte i = 0; i < bufferSize; i++) {
      result += buffer[i];
  }
  return result;
}
