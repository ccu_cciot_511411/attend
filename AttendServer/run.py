from attendserver import MainSystem


HOST = '172.26.6.191'
WEB_API_PORT = 5566
DEVICE_API_PORT = 8964


def main():
    main_sys = MainSystem(host=HOST, web_api_port=WEB_API_PORT, device_api_port=DEVICE_API_PORT)
    main_sys.run()


if __name__ == "__main__":
    main()