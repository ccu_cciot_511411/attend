# RFID考勤系統 Server端

負責串接設備端、數據庫、網頁端。

於Server端運行本程式後，將以TCP與HTTP協定分別搭建設備端與網頁端的API口，提供紀錄、查詢等服務。

## Getting Started 使用指南

### Prerequisites 项目使用条件

#### 硬體需求
    具備聯網功能的電腦

#### 軟體需求
    Ubuntu 18.x (推薦)
---
### Installation 安装

安裝分為`完整安裝`與`僅安裝Python依賴包`兩種

### 完整安裝Python與相關依賴包 (推薦Linux, MacOS)
在console中，執行shell腳本
```sh
sh install.sh
```
* 此方法需要管理員授權


### 僅安裝Python依賴包
若已安裝Python3，可於當前目錄底下，執行以下命令安裝Python依賴包
```sh
pip3 install -r requirements.txt
```
---



### 啟動Server

於當前目錄底下，執行以下命令啟動本Server端程式

```sh
python3 run.py
```

若啟動成功，會看到以下回應
![image](imgs/run.png)

### 結束Server

為確保線程與網路端口正常釋放，請嘗試按下`Ctrl + C`並等待3秒，待程式正常關閉。
若正常關閉，會看到以下回應
![image](imgs/close.png)