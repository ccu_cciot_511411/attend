#!/bin/bash

# Install dependencies
sudo apt-get update
sudo apt-get install -y python3-pip
sudo apt-get install -y python3-venv
sudo apt-get install -y python3-dev
sudo apt-get install -y libpq-dev
sudo apt-get install -y nginx
sudo apt-get install -y gunicorn3

# Create virtual environment
python3 -m venv venv
source venv/bin/activate

# Install python dependencies
pip3 install -r requirements.txt
echo "Server Dependencies installed"

