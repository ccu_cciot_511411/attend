import requests

server_address = "127.0.0.1"
port = 8899


# 客户端参数
user_id = "00000000"
start_time = "2023-10-01-00:00:00"
end_time = "2023-10-10-00:00:00"

# 构建GET请求的URL
url = f"http://{server_address}:{port}/?user_id={user_id}&start_time={start_time}&end_time={end_time}"

# 发送GET请求
response = requests.get(url)

# 处理服务器响应
if response.status_code == 200:
    result = response.text
    print(result)
else:
    print(f"Error: {response.status_code}")
