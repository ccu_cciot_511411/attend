from src import MainSystem



def main():
    host = '127.0.0.1'
    web_api_port = 5566
    main_sys = MainSystem(host=host)
    main_sys.run()


if __name__ == "__main__":
    main()
