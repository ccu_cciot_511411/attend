import time

from src.web_api import WebAPISocketThread, WebAPITestThread, WebAPIHTTPThread


def main():
    server_alive = True
    # server = WebAPISocketThread(
    #     host="127.0.0.1", port=8899, alive_func=lambda: server_alive
    # )
    server = WebAPIHTTPThread(
        host="127.0.0.1", port=8899, alive_func=lambda: server_alive
    )
    # server.setDaemon(True)
    server.start()
    _ = input()


if __name__ == "__main__":
    main()
