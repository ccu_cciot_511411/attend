import boto3
import time
import datetime as dt
from datetime import timedelta
from boto3.dynamodb.conditions import Key, Attr
from functools import wraps
from typing import List, Dict


AWS_ACCESSS_KEY_ID = "AKIAQTQO47WRBS54XSIH"
AWS_SECRET_ACCESS_KEY = "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"
REGION_NAME = "ap-northeast-1"
USER_TABLE_NAME = "AttendUser"
RECORD_TABLE_NAME = "AttendRecord"


def dynamodb_connect_hanndler(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        counter = 3
        while counter > 0:
            try:
                return func(*args, **kwargs)
            except RuntimeError as e:
                print(f"DynamoDB Connection Error: {e}")
                counter -= 1
                time.sleep(0.5)
        print(
            f"DynamoDB Error: Failed to connect to DynamoDB while executing: {func.__name__}"
        )

    return wrapper


def _str_dt_2_dt(str_dt: str) -> dt.datetime:
    return dt.datetime.strptime(f"{str_dt}", "%Y-%m-%d %H:%M:%S")


def _dt_2_str(dt: dt.datetime) -> str:
    return dt.strftime("%Y-%m-%d %H:%M:%S")


def _dt_2_str_date(dt: dt.datetime) -> str:
    return dt.strftime("%Y-%m-%d")


class DynamoDB:
    def __init__(self):
        self.__implement_dynamodb_instance()
        self.__implement_tables()

    def __implement_dynamodb_instance(self):
        self.__instance = boto3.Session(
            aws_access_key_id=AWS_ACCESSS_KEY_ID,
            aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
            region_name=REGION_NAME,
        )
        self.__dynamodb = self.__instance.resource("dynamodb")

    def __implement_tables(self):
        self.table_record = self.__dynamodb.Table(RECORD_TABLE_NAME)
        self.table_user = self.__dynamodb.Table(USER_TABLE_NAME)

    def query_rfid(self, rfid_value: str):
        try:
            response = self.table_user.scan(
                FilterExpression=Attr("rfid").eq(rfid_value)
            )
            items = response["Items"]
            if len(items) == 0:
                print("No matching item found")
                return None
            else:
                return items
        except Exception as e:
            print("Query Error:", e)
            return None

    def insert_rfid(self, rfid_value: str):
        try:
            print(rfid_value)
            self.table_record.put_item(
                Item={
                    "rfid": rfid_value,
                    "datetime": (dt.datetime.utcnow() + timedelta(hours=8)).strftime(
                        "%Y-%m-%d %H:%M:%S"
                    ),
                }
            )
            print("data insert successful")
        except Exception as e:
            print("Error:", e)

    def query_record(
        self, rfid: str, start: dt.datetime = None, end: dt.datetime = None
    ) -> List[Dict]:
        """Query the record of the user with the given rfid

        Args:
            rfid (str): RFID of the user
            start (dt.datetime, optional): Start datetime. Defaults to None.
            end (dt.datetime, optional): End datetime. Defaults to None.

        Returns:
            List[Dict]: List of datetime in the format of {"rfid": rfid, "datetime": datetime}
        """
        try:
            condition = Key("rfid").eq(rfid)
            if not start:
                start = dt.datetime(1970, 1, 1)
            if not end:
                end = dt.datetime.now() + dt.timedelta(seconds=1)
            start_date = _dt_2_str(start)
            end_date = _dt_2_str(end)
            condition = condition & Key("datetime").between(start_date, end_date)
            response = self.table_record.query(KeyConditionExpression=condition)
            if "Items" in response:
                out = response["Items"]
                for i in out:
                    str_datetime = i["datetime"]
                    datetime = _str_dt_2_dt(str_datetime)
                    i["datetime"] = datetime
                return out
            else:
                return None
        except Exception as e:
            print("Query Error:", e)
            return None

    def query_user(self, user_id: int) -> List:
        """Query the user with the given user_id

        Args:
            user_id (int): user_id
        Returns:
            List[Dict]: List of user info in the format of {"user_id": user_id, "rfid": rfid}
        """
        try:
            condition = Key("user_id").eq(user_id)
            response = self.table_user.query(KeyConditionExpression=condition)
            if "Items" in response:
                out = response["Items"]
                return out
            else:
                return None
        except Exception as e:
            print("Query Error:", e)
            return None

    def query_user_record(
        self, user_id: int, start: dt.datetime = None, end: dt.datetime = None
    ) -> List:
        """Query the record of the user with the given user_id

        Args:
            user_id (int): user_id
            start (dt.datetime, optional): Start datetime. Defaults to None.
            end (dt.datetime, optional): End datetime. Defaults to None.

        Returns:
            List[Dict]: List of datetime in the format of {"rfid": rfid, "datetime": datetime}
        """
        user_infos = self.query_user(user_id)
        if user_infos is None or len(user_infos) == 0:
            print("User not found")
            return None
        user_rfids = [user_info["rfid"] for user_info in user_infos]
        user_records = []
        if user_rfids:
            for rfid in user_rfids:
                user_records += self.query_record(rfid, start, end)
        return user_records

    @dynamodb_connect_hanndler
    def add_datetime(self, rfid: str, datetime: dt.datetime):
        assert isinstance(
            datetime, dt.datetime
        ), "datetime must be datetime.datetime type"
        dt_str = _dt_2_str(datetime)
        self.table_record.put_item(Item={"rfid": rfid, "datetime": dt_str})
        return True
