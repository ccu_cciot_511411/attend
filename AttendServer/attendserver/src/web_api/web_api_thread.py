import socket
import time
import threading
import datetime
import json


from typing import Callable, List, Tuple
from .methods import query_user_attend_str

class WebAPISocketThread(threading.Thread):
    def __init__(self, host:str, port:int, alive_func:Callable):
        super().__init__()
        self.host = host
        self.port = port
        self.socket = None
        self.alive_fun = alive_func


    def run(self):
        self.running = True
        self.__start_server()


    def __start_server(self):
        print('Starting Web API server...')
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            self.socket = s
            self.socket.setblocking(False)
            self.socket.bind((self.host, self.port))
            self.__listen_and_process()
            self.socket.close()
            print('Web API socket closed')


    def __respond(self, conn:socket.socket, data:str):
        conn.sendall(data.encode())
        return



    def __parse_query_req(self, req:List[str])->Tuple[str, datetime.datetime, datetime.datetime]:
        """
        parse query request into user_id, start, end

        Args:
            req (List[str]): req should be formatted as:
                ['user_id', 'start', 'end']
                user_id: str as 8-digit integer
                start: str as '%Y-%m-%d-%H:%M:%S'
                end: str as '%Y-%m-%d-%H:%M:%S'
        Returns:
            Tuple[str, datetime.datetime, datetime.datetime]: user_id, start, end
        
        Examples:
            >>> req = ['00000000', '2023-10-01-00:00:00', '2023-10-07-00:00:00']
            >>> user_id, start, end = __parse_query_req(req)
            >>> user_id
            '00000000'
            >>> start
            datetime.datetime(2023, 10, 1, 0, 0)
            >>> end
            datetime.datetime(2023, 10, 7, 0, 0)
        """
        assert len(req) == 3, 'Request should be formatted as: [user_id, start, end]'
        assert len(req[0]) == 8, 'user_id should be 8-digit integer'
        user_id = req[0]
        try:
            start = datetime.datetime.strptime(req[1], '%Y-%m-%d-%H:%M:%S')
            end = datetime.datetime.strptime(req[2], '%Y-%m-%d-%H:%M:%S')
        except ValueError:
            print('Invalid date format')
            return None, None, None
        return user_id, start, end



    def __query_attendance(self, req:List[str])->str:
        """Return attendance query result as json string

        Args:
            req (List[str]): req should be formatted as:
                ['user_id', 'start', 'end']
                user_id: str as 8-digit integer
                start: str as '%Y-%m-%d-%H:%M:%S'
                end: str as '%Y-%m-%d-%H:%M:%S'
        Returns:
            str: json string
        
        Examples:
            >>> req = ['00000000', '2023-10-01-00:00:00', '2023-10-07-00:00:00']
            >>> res = __query_attendance(req)
            >>> print(res)
            {
                "2023-10-01": {
                    "attend": "2023-10-01-08:00:00",
                    "leave": "2023-10-01-17:00:00",
                    "valid": true,
                    "records": [
                        "2023-10-01-08:00:00",
                        "2023-10-01-12:00:00",
                        "2023-10-01-13:00:00",
                        "2023-10-01-17:00:00"
                    ]
                },
                "2023-10-02": {
                    "attend": "2023-10-02-08:00:00",
                    "leave": "2023-10-02-17:00:00",
                    "valid": true,
                    "records": [
                        "2023-10-02-08:00:00",
                        "2023-10-02-12:00:00",
                        "2023-10-02-13:00:00",
                        "2023-10-02-17:00:00"
                    ]
                },
                "2023-10-03": {
                    "attend": "2023-10-03-08:00:00",
                    "leave": "2023-10-03-17:00:00",
                    "valid": true,
                    "records": [
                        "2023-10-03-08:00:00",
                        "2023-10-03-12:00:00",
                        "2023-10-03-13:00:00",
                        "2023-10-03-17:00:00"
                    ]
                },
                "2023-10-04": {
                    "attend": "2023-10-04-08:00:00",
                    "leave": "2023-10-04-17:00:00",
                    "valid": true,
                    "records": [
                        "2023-10-04-08:00:00",
                        "2023-10-04-12
            ...
        """
        user_id, start, end = self.__parse_query_req(req)
        if user_id is None or start is None or end is None:
            return
        res = query_user_attend_str(user_id, start, end)
        return res


    def __parse_and_process_req(self, data:bytes, conn:socket.socket)->None:
        """Parse and process request

        Args:
            data (bytes): request data
            conn (socket.socket): connected socket
        """
        data = data.decode()
        print(f'Web Port received:\n{data}\nFrom:{conn.getpeername()}\n\n'+('-'*20))
        if data == 'close yourself':
            self.running = False  # stop the server
            return
        data = data.split('/')
        if data[0] == 'query':
            res = self.__query_attendance(data[1:])
            if res is None:
                self.__respond(conn, 'Invalid request')
                return
            self.__respond(conn, res)
            return


    def __listen_and_process(self):
        self.socket.listen()
        while self.alive_fun():
            try:
                conn, addr = self.socket.accept()  # connected socket, address
                with conn:
                    while True:
                        data = conn.recv(1024)
                        if not data:
                            break
                        self.__parse_and_process_req(data=data, conn=conn)
            except BlockingIOError:
                continue
        

class WebAPI:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.socket_thread = None
        self.__is_running = False

    def start(self):
        self.__is_running = True
        self.socket_thread = WebAPISocketThread(self.host, self.port, lambda: self.__is_running)
        self.socket_thread.setDaemon(True)
        self.socket_thread.start()

    def close(self):
        self.__is_running = False
        self.socket_thread.join()
        self.socket_thread = None



class WebAPITestThread(threading.Thread):
    def __init__(self, host:str, port:int):
        super().__init__()
        self.host = host
        self.port = port


    def run(self):
        self.__start_client()


    def __start_client(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            buffer_len = 1024
            print('Web API Testing....')
            s.connect((self.host, self.port))
            print(f'Testing query funciton...')
            s.sendall(b'query/00000000/2023-10-01-00:00:00/2023-10-07-00:00:00')
            data = s.recv(1024)
            print('Received:\n', repr(data))
            s.close()
        print('Finished testing.')

    


if __name__ == "__main__":
    server = WebAPI(host="127.0.0.1", port=5114)
    time.sleep(1)
    client = WebAPIClientThread(host="127.0.0.1", port=5114)
    server.start()
    client.start()
    time.sleep(5)
    server.close()


