import requests


def getter_http(ip, port, data):
    url = f"http://{ip}:{port}/"
    try:
        r = requests.get(url, data=data)
        return r.text
    except Exception as e:
        print(e)
        return None
