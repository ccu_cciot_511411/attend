import datetime
import holidays

import os
import json

from typing import Dict, List, Tuple

from ..database import DynamoDB


def _parse_json_file(json_dict: dict):
    out = {"once": {}, "weekly": {}, "monthly": {}, "yearly": {}}
    for freq, holiday_dict in json_dict.items():
        if freq == "once":
            for holiday_name, date in holiday_dict.items():
                out[freq][
                    datetime.datetime.strptime(date, "%Y-%m-%d").date()
                ] = holiday_name
        elif freq == "weekly":
            for holiday_name, day_ls in holiday_dict.items():
                for day in day_ls:
                    out[freq][day] = holiday_name

        elif freq == "monthly":
            for holiday_name, date in holiday_dict.items():
                out[freq][date] = holiday_name

        elif freq == "yearly":
            for holiday_name, date in holiday_dict.items():
                out[freq][date] = holiday_name
        else:
            raise ValueError(f"Unknown frequency: {freq}")
    return out


def _is_custom_holiday(date: datetime.datetime, custom_holidays: dict):
    if date.date() in custom_holidays["once"]:
        return custom_holidays["once"][date.date()]
    if date.weekday() in custom_holidays["weekly"]:
        return custom_holidays["weekly"][date.weekday()]
    if date.date().strftime("%d") in custom_holidays["monthly"]:
        return custom_holidays["monthly"][date.date().strftime("%d")]
    if date.date().strftime("%m-%d") in custom_holidays["yearly"]:
        return custom_holidays["yearly"][date.date().strftime("%m-%d")]
    return None


def _read_and_parse_custom_holidays():
    current_dir = os.path.dirname(os.path.abspath(__file__))
    dir_name = current_dir.split("src")[0] + "data"
    file_name = "custom_holidays.json"
    file_path = os.path.join(dir_name, file_name)
    with open(file_path, "r") as f:
        json_file = json.load(f)
    return _parse_json_file(json_file)


def _is_holiday(date: datetime.datetime):
    tw_holidays = holidays.TW()
    if date.date() in tw_holidays:
        return tw_holidays[date.date()]
    custom_holidays = _read_and_parse_custom_holidays()
    custom_holiday = _is_custom_holiday(date=date, custom_holidays=custom_holidays)
    return custom_holiday


def _get_rest_hour() -> List[Tuple[datetime.datetime, datetime.datetime]]:
    current_dir = os.path.dirname(os.path.abspath(__file__))
    dir_name = current_dir.split("src")[0] + "data"
    file_name = "rest_hour.json"
    file_path = os.path.join(dir_name, file_name)
    with open(file_path, "r") as f:
        json_file = json.load(f)
    out = [
        (
            datetime.datetime.strptime(time["start"], "%H:%M"),
            datetime.datetime.strptime(time["end"], "%H:%M"),
        )
        for _, time in json_file.items()
    ]
    return out


def _cal_time_difference(time1: datetime.datetime, time2: datetime.datetime) -> int:
    time1 = time1.time()
    time2 = time2.time()
    time1 = datetime.datetime.strptime(str(time1), "%H:%M:%S")
    time2 = datetime.datetime.strptime(str(time2), "%H:%M:%S")
    return (time2 - time1).seconds // 60


def _get_calendar(start: datetime.datetime, end: datetime.datetime) -> dict:
    out = {}
    while start <= end:
        out[start.date()] = {
            "attend": None,
            "leave": None,
            "work_time": None,
            "records": [],
            "holiday": _is_holiday(start),
        }
        start += datetime.timedelta(days=1)
    return out


def _allocate_records_into_calendar(records: List[dict], calendar: dict) -> dict:
    records.sort(key=lambda x: x["datetime"])
    for record in records:
        record_date = record["datetime"].date()
        calendar[record_date]["records"].append(record["datetime"])
    return calendar


def _cal_valid_work_time_in_day(date_records: List[datetime.datetime]):
    if len(date_records) == 0:
        return 0
    if len(date_records) < 2:
        return 0
    rest_hours = _get_rest_hour()
    date_records.sort()
    valid_time = [0] * 2
    for record in date_records:
        if record.time() < datetime.time(6, 0):
            continue  # record time is earlier than 6:00
        if (
            record.time() < rest_hours[0][0].time()
        ):  # record time is earlier than rest start time
            valid_time[0] = max(
                valid_time[0], _cal_time_difference(record, rest_hours[0][0])
            )
        elif (
            record.time() > rest_hours[0][1].time()
        ):  # record time is later than rest stop time
            valid_time[1] = max(
                valid_time[1], _cal_time_difference(rest_hours[0][1], record)
            )
    return sum(valid_time)


def _cal_work_time(calendar: dict) -> dict:
    for _, date_dict in calendar.items():
        date_dict["work_time"] = _cal_valid_work_time_in_day(date_dict["records"])
        date_dict["valid"] = (
            date_dict["work_time"] >= 8 * 60 or date_dict["holiday"] != None
        )
        date_dict["attend"] = (
            date_dict["records"][0] if len(date_dict["records"]) > 0 else None
        )
        date_dict["leave"] = (
            date_dict["records"][-1] if len(date_dict["records"]) > 1 else None
        )
    return calendar


def json_query_res(res: dict) -> dict:
    out = {}
    for date, date_dict in res.items():
        out[date.strftime("%Y-%m-%d")] = {}
        for key, value in date_dict.items():
            if key == "records":
                out[date.strftime("%Y-%m-%d")][key] = [
                    i.strftime("%Y-%m-%d-%H:%M:%S") for i in value
                ]
            elif key == "attend" or key == "leave":
                out[date.strftime("%Y-%m-%d")][key] = (
                    value.strftime("%Y-%m-%d-%H:%M:%S") if value else None
                )
            else:
                out[date.strftime("%Y-%m-%d")][key] = value
    return out


def str_json_query_res(res: dict):
    return json.dumps(res, indent=4, ensure_ascii=True)


def query_user_attend(
    user_id: str, start: datetime.datetime = None, end: datetime.datetime = None
) -> Dict[datetime.datetime, Dict[datetime.datetime, datetime.datetime]]:
    """
    Return: Dict
    {
        datetime:{
            'attend': datetime
            'leave': datetime
            'valid': bool
            'records': list
        }
    }
    """
    db = DynamoDB()
    record = db.query_user_record(user_id, start, end)
    if record is None:
        return None
    calendar = _get_calendar(start=start, end=end)
    calendar = _allocate_records_into_calendar(record, calendar)
    calendar = _cal_work_time(calendar)
    return calendar


def query_user_attend_str(
    user_id: str, start: datetime.datetime = None, end: datetime.datetime = None
) -> str:
    """
    Query user attendance and return json string

    Args:
        user_id (int): user id
        start (datetime.datetime, optional): start date. Defaults to None.
        end (datetime.datetime, optional): end date. If None, end date is set to today. Defaults to None.

    Returns:
        str: json string

    """
    res = query_user_attend(user_id, start, end)
    if res is None:
        return None
    res = json_query_res(res)
    json_res = json.dumps(res, indent=4, ensure_ascii=True)
    return json_res


if __name__ == "__main__":
    date = datetime.datetime(2023, 11, 11)
    custom_holidays = _read_and_parse_custom_holidays()
    print(_is_custom_holiday(date, custom_holidays))
    rest_hours = _get_rest_hour()
    print(rest_hours)
    now = datetime.datetime.now()
    print(now.time() > rest_hours[0][1].time())
