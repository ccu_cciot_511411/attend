import http.server
import threading
import datetime

from urllib.parse import urlparse, parse_qs
from typing import Callable, Tuple

from .methods import query_user_attend_str


class Handler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        print(f"GET request, from: {self.client_address}")
        parsed_path = urlparse(self.path)
        query_params = parse_qs(parsed_path.query)
        print(f"Query params: {query_params}")
        user_id, start_time, end_time = self.__extract_query_params(query_params)
        res = self.__query_attend(user_id, start_time, end_time)
        self.__reply(res)
    
    def __extract_query_params(self, query_params: dict) -> Tuple[str, str, str]:
        """
        extract the query params from the query_params dict
        """
        user_id = query_params.get("user_id", [""])[0]
        start_time = query_params.get("start_time", [""])[0]
        end_time = query_params.get("end_time", [""])[0]
        return (user_id, start_time, end_time)

    def __add_ac_header(self)->None:
        """
        this method solves the CORS problem
        """
        self.send_header('Access-Control-Allow-Origin', '*')  # 允许所有来源的请求
        self.send_header('Access-Control-Allow-Methods', 'GET')  # 允许GET请求
        self.send_header('Access-Control-Allow-Headers', 'Content-Type')  # 允许Content-Type头


    def __convert_time(
        self, start_time: str, end_time: str
    ) -> Tuple[datetime.datetime, datetime.datetime]:
        try:
            start = datetime.datetime.strptime(start_time, "%Y-%m-%d-%H:%M:%S")
            end = datetime.datetime.strptime(end_time, "%Y-%m-%d-%H:%M:%S")
            return (start, end)
        except ValueError:
            print("Invalid datetime format")

    def __query_attend(self, user_id: str, start_time: str, end_time: str) -> str:
        start_time, end_time = self.__convert_time(start_time, end_time)
        res = query_user_attend_str(user_id, start_time, end_time)
        return res
    

    def __reply(self, res: str) -> None:
        """
        reply the result to the client
        """
        if res is None:
            res = "user not found"
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.__add_ac_header()
        self.end_headers()
        self.wfile.write(res.encode())


class WebAPIHTTPThread(threading.Thread):
    def __init__(self, host: str, port: int, alive_func: Callable, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.host = host
        self.port = port
        self.alive_fun = alive_func
        self.setDaemon(True)

    def run(self):
        server_addr = (self.host, self.port)
        http_d = http.server.HTTPServer(server_addr, Handler)
        print(f"Starting web query server on {server_addr[0]}:{server_addr[1]}...\n")
        http_d.serve_forever()
