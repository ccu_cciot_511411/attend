import socket
import time
import threading

from typing import Callable
from ..database import DynamoDB

class DeviceAPISocketThread(threading.Thread):
    def __init__(self, host:str, port:int, alive_func:Callable):
        super().__init__()
        self.host = host
        self.port = port
        self.socket = None
        self.alive_fun = alive_func
        self.database = DynamoDB()

    def run(self):
        self.running = True
        self.__start_server()


    def __start_server(self):
        print('Starting Device API server...')
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            self.socket = s
            self.socket.setblocking(False)
            self.socket.bind((self.host, self.port))
            self.__listen_and_process()
            self.socket.close()  
            print('Device API socket closed')


    def __listen_and_process(self):
        self.socket.listen()
        while self.alive_fun():
            try:
                conn, addr = self.socket.accept()  # connected socket, address
                with conn:
                    while True:
                        data = conn.recv(1024)
                        if not data:
                            break
                        self.__parse_data(data, conn)
                    conn.close()
            except BlockingIOError:
                continue
        

    def __parse_data(self, data:bytes, conn:socket.socket):
        """
        """
        data = data.decode().strip().replace(' ','')
        print(f'Device Port received:\n{data}\nFrom:{conn.getpeername()}\n\n'+('-'*20))
        result = self.database.query_rfid(data)
        if result == None:
            conn.send(b'999')
        else:
            conn.send(b'777')
            self.database.insert_rfid(data)

        if data == 'close yourself':
            self.running = False  # stop the server
            return
        

class DeviceAPI:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.socket_thread = None
        self.__is_running = False

    def start(self):
        self.__is_running = True
        self.socket_thread = DeviceAPISocketThread(self.host, self.port, lambda: self.__is_running)
        #self.socket_thread.setDaemon(True)
        self.socket_thread.start()

    def close(self):
        self.__is_running = False
        self.socket_thread.join()
        self.socket_thread = None



