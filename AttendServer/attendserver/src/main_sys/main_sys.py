from threading import Thread
from multiprocessing.dummy import Pool
from ..database import DynamoDB
# from ..web_api import WebAPISocketThread
from ..web_api import WebAPIHTTPThread
from ..device_api import DeviceAPISocketThread


from typing import List



class MainSystem:
    def __init__(self, host:str, web_api_port:int, device_api_port:int):
        self.__host = host
        self.__web_api_port  = web_api_port
        self.__device_api_port = device_api_port
        self.__build_threads()


    def __build_web_api_thread(self) -> Thread:
        self.__is_web_api_running = True
        out = WebAPIHTTPThread(host=self.__host, port= self.__web_api_port, alive_func=lambda:self.__is_web_api_running)
        out.setDaemon(True)
        return out

    def __build_device_api_thread(self) -> Thread:
        self.__is_device_api_running = True
        out = DeviceAPISocketThread(host=self.__host, port= self.__device_api_port, alive_func=lambda:self.__is_device_api_running)
        out.setDaemon(True)
        return out

    def __build_threads(self):
        self.__threads = []
        self.__web_api_thread = self.__build_web_api_thread()
        self.__device_api_thread = self.__build_device_api_thread()
        self.__threads.append(self.__web_api_thread)
        self.__threads.append(self.__device_api_thread)


    def run(self):
        """
        Start all threads and wait for keyboard interrupt.
        """
        for thread in self.__threads:
            thread.start()
        try:
            print('All threads have been started.')
            for thread in self.__threads:
                thread.join()
        except KeyboardInterrupt:
            print('Stop system by keyboard interrupt.')
        print('System closed')
