from src.web_api import WebAPITestThread


def main():
    host = '52.198.1.216'
    port = 5566
    test_thread = WebAPITestThread(host=host, port=port)
    test_thread.start()
    test_thread.join()

if __name__ == "__main__":
    main()
